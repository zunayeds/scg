﻿namespace SCG
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMethodName = new System.Windows.Forms.TextBox();
            this.txtReturnType = new System.Windows.Forms.TextBox();
            this.txtServiceName = new System.Windows.Forms.TextBox();
            this.cboMethodType = new System.Windows.Forms.ComboBox();
            this.cboHTTPActionType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtViewModel = new System.Windows.Forms.TextBox();
            this.txtOutput = new System.Windows.Forms.RichTextBox();
            this.chkWriteToFile = new System.Windows.Forms.CheckBox();
            this.txtInputParams = new System.Windows.Forms.TextBox();
            this.txtVMProperties = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtMethodName
            // 
            this.txtMethodName.Location = new System.Drawing.Point(139, 12);
            this.txtMethodName.Name = "txtMethodName";
            this.txtMethodName.Size = new System.Drawing.Size(292, 20);
            this.txtMethodName.TabIndex = 0;
            // 
            // txtReturnType
            // 
            this.txtReturnType.Location = new System.Drawing.Point(139, 38);
            this.txtReturnType.Name = "txtReturnType";
            this.txtReturnType.Size = new System.Drawing.Size(292, 20);
            this.txtReturnType.TabIndex = 1;
            // 
            // txtServiceName
            // 
            this.txtServiceName.Location = new System.Drawing.Point(139, 64);
            this.txtServiceName.Name = "txtServiceName";
            this.txtServiceName.Size = new System.Drawing.Size(292, 20);
            this.txtServiceName.TabIndex = 2;
            // 
            // cboMethodType
            // 
            this.cboMethodType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMethodType.FormattingEnabled = true;
            this.cboMethodType.Items.AddRange(new object[] {
            "Basic",
            "Transaction",
            "Report"});
            this.cboMethodType.Location = new System.Drawing.Point(139, 118);
            this.cboMethodType.Name = "cboMethodType";
            this.cboMethodType.Size = new System.Drawing.Size(292, 21);
            this.cboMethodType.TabIndex = 4;
            // 
            // cboHTTPActionType
            // 
            this.cboHTTPActionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHTTPActionType.FormattingEnabled = true;
            this.cboHTTPActionType.Items.AddRange(new object[] {
            "POST",
            "GET",
            "DELETE"});
            this.cboHTTPActionType.Location = new System.Drawing.Point(139, 145);
            this.cboHTTPActionType.Name = "cboHTTPActionType";
            this.cboHTTPActionType.Size = new System.Drawing.Size(292, 21);
            this.cboHTTPActionType.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Method Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Return Type";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Service Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Method Type";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "HTTP Action Type";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Input Parameters";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(184, 245);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 12;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "ViewModel Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtViewModel
            // 
            this.txtViewModel.Location = new System.Drawing.Point(139, 91);
            this.txtViewModel.Name = "txtViewModel";
            this.txtViewModel.Size = new System.Drawing.Size(292, 20);
            this.txtViewModel.TabIndex = 3;
            this.txtViewModel.TextChanged += new System.EventHandler(this.txtViewModel_TextChanged);
            // 
            // txtOutput
            // 
            this.txtOutput.AcceptsTab = true;
            this.txtOutput.Location = new System.Drawing.Point(12, 282);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtOutput.Size = new System.Drawing.Size(419, 96);
            this.txtOutput.TabIndex = 16;
            this.txtOutput.Text = "";
            // 
            // chkWriteToFile
            // 
            this.chkWriteToFile.AutoSize = true;
            this.chkWriteToFile.Location = new System.Drawing.Point(14, 247);
            this.chkWriteToFile.Name = "chkWriteToFile";
            this.chkWriteToFile.Size = new System.Drawing.Size(86, 17);
            this.chkWriteToFile.TabIndex = 17;
            this.chkWriteToFile.Text = "Write To File";
            this.chkWriteToFile.UseVisualStyleBackColor = true;
            // 
            // txtInputParams
            // 
            this.txtInputParams.Location = new System.Drawing.Point(139, 173);
            this.txtInputParams.Name = "txtInputParams";
            this.txtInputParams.Size = new System.Drawing.Size(292, 20);
            this.txtInputParams.TabIndex = 6;
            // 
            // txtVMProperties
            // 
            this.txtVMProperties.Location = new System.Drawing.Point(139, 199);
            this.txtVMProperties.Name = "txtVMProperties";
            this.txtVMProperties.Size = new System.Drawing.Size(292, 20);
            this.txtVMProperties.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "ViewModel properties";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 391);
            this.Controls.Add(this.txtVMProperties);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtInputParams);
            this.Controls.Add(this.chkWriteToFile);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtViewModel);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboHTTPActionType);
            this.Controls.Add(this.cboMethodType);
            this.Controls.Add(this.txtServiceName);
            this.Controls.Add(this.txtReturnType);
            this.Controls.Add(this.txtMethodName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "STS Code Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMethodName;
        private System.Windows.Forms.TextBox txtReturnType;
        private System.Windows.Forms.TextBox txtServiceName;
        private System.Windows.Forms.ComboBox cboMethodType;
        private System.Windows.Forms.ComboBox cboHTTPActionType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtViewModel;
        private System.Windows.Forms.RichTextBox txtOutput;
        private System.Windows.Forms.CheckBox chkWriteToFile;
        private System.Windows.Forms.TextBox txtInputParams;
        private System.Windows.Forms.TextBox txtVMProperties;
        private System.Windows.Forms.Label label8;
    }
}

