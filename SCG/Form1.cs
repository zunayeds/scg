﻿using System;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace SCG
{
    public partial class Form1 : Form
    {
        string methodName, returnType, serviceName, methodType, dataType, variable, vmVariable, extractedVariables, clientVMmapping, viewModelName, viewModelVariableName, httpActionType, inputParams, variables, vmVariables, accessLayerServiceName, accessLayerMethodName, generatedContent, tempContent;
        int paramCount;
        string location, accessLayerFileName, routeFileName, wcfServiceInterfaceFileName, wcfServiceFileName, apiControllerFileName, clientServiceFileName, clientInterfaceFileName, httpServiceFileName, fileContent, filePath;
        int index, index1, index2, index3, index4;
        bool isVMused, isMappedVM, isWriteToFile;
        Dictionary<string, string> parameters;
        List<string> vmProperties;
        const string autoGenStartRegion = "#region Auto-Generated";
        const string autoGenEndRegion = "#endregion";

        public Form1()
        {
            InitializeComponent();
            location = ConfigurationSettings.AppSettings["Location"] + "trunk\\TheSTS\\STS\\";
            cboMethodType.SelectedIndex = 0;
            cboHTTPActionType.SelectedIndex = 0;
            txtVMProperties.Enabled = false;
            routeFileName = "TheSTS.WebAPI.RouteFactory\\RouteBucket.cs";
            httpServiceFileName = "TheSTS.WCF.WebHost\\HttpService.svc.cs";
            //WriteText(); // C# to Angular
        }
        public void WriteText()
        {
            const string methodHead = "public static async Task ";
            const string oncomplete = "OnCompletion<";
            string fileText = File.ReadAllText("D:\\LOCAL\\STS_SVN\\trunk\\TheSTS\\STS\\TheSTS\\APIService\\BasicService.cs");
            int index = fileText.IndexOf(methodHead);
            int bracketIndex = fileText.Substring(index).IndexOf('(');
            int onCompleteIndex = fileText.Substring(index).IndexOf(oncomplete);
            int onCompleteStartIndex, onCompleteEndIndex;
            int actionTypeIndex, lastCommaIndex, routeIndex;
            string methodName, returnParam;
            string[] paramList, paramType;
            string parameters, paramNames;
            string final = string.Empty;
            string actionType = string.Empty;
            string route = string.Empty;
            string rawParam = string.Empty;
            index = fileText.IndexOf(methodHead);
            fileText = fileText.Substring(index);
            while (fileText.Contains(methodHead))
            {
                index++;
                try
                {
                    methodName = fileText.Substring(methodHead.Length - 1, bracketIndex - methodHead.Length + 1).Trim();
                    if (methodName.EndsWith("Async")) methodName = methodName.Substring(0, methodName.Length - 5);
                    parameters = string.Empty;
                    paramNames = string.Empty;
                    if (onCompleteIndex - bracketIndex > 1)
                    {
                        lastCommaIndex = fileText.Substring(0, onCompleteIndex).LastIndexOf(',');
                        rawParam = fileText.Substring(bracketIndex + 1, lastCommaIndex - bracketIndex + 1).TrimEnd(' ').Trim(',');
                        if (!rawParam.Contains("Dictionary"))
                        {
                            paramList = rawParam.Split(',').Select(s => s.Trim()).ToArray();
                            foreach (string param in paramList)
                            {
                                paramType = param.Split(' ').Select(s => s.Trim()).ToArray();
                                if (!string.IsNullOrWhiteSpace(parameters))
                                {
                                    parameters += ", ";
                                    paramNames += ", ";
                                }
                                parameters += (paramType[0].EndsWith("?") ? paramType[1] + "?" : paramType[1]) + ": " + CtoA(paramType[0].TrimEnd('?'));
                                paramNames += paramType[1];
                            }
                        }
                    }
                    onCompleteStartIndex = fileText.Substring(onCompleteIndex).IndexOf('<');
                    onCompleteEndIndex = fileText.Substring(onCompleteIndex, 100).LastIndexOf('>');
                    returnParam = fileText.Substring(onCompleteIndex + onCompleteStartIndex + 1, onCompleteEndIndex - onCompleteStartIndex - 1).Trim();
                    actionTypeIndex = fileText.IndexOf("EnumHTTPVerb.");
                    actionType = fileText.Substring(actionTypeIndex + 13, fileText.Substring(actionTypeIndex).IndexOf(',') - 13).ToLower();
                    routeIndex = fileText.Substring(actionTypeIndex).IndexOf(",") + 1;
                    route = fileText.Substring(actionTypeIndex + routeIndex, fileText.Substring(actionTypeIndex + routeIndex).IndexOf(",")).Trim().Replace("RouteBucket.", "this.routes.");

                    final += methodName + "(" + parameters + "): Observable<" + CtoA(returnParam) + "> {\n";
                    final += "\treturn this.httpService." + actionType + "<" + CtoA(returnParam) + @">(this.routes.Route_Prefix_BasicService + ""/"" + " + route + (!string.IsNullOrWhiteSpace(paramNames) ? string.Concat(", { ", paramNames, " }") : "") + ");\n}\n\n";

                    fileText = fileText.Substring(index);
                    index = fileText.IndexOf(methodHead);
                    fileText = fileText.Substring(index);
                    bracketIndex = fileText.IndexOf('(');
                    onCompleteIndex = fileText.IndexOf(oncomplete);
                }
                catch (Exception ex)
                {
                    break;
                }
            }
            final += "\n";
        }
        string CtoA(string dataType)
        {
            string lowerVersion = dataType.ToLower();
            switch (lowerVersion)
            {
                case "int":
                case "short":
                case "double":
                case "decimal":
                case "int16":
                case "int32":
                case "int64":
                    return "number";
                case "string":
                    return "string";
                case "bool":
                    return "boolean";
                default:
                    if (dataType.StartsWith("List<"))
                    {
                        return dataType.Substring(5, dataType.Length - 5 - 1).Trim() + "[]";
                    }
                    return dataType;
            }
        }
        private void txtViewModel_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtViewModel.Text.Trim())) txtVMProperties.Enabled = false;
            else txtVMProperties.Enabled = true;
        }

        string GetNewLineAndTab(int tabCount, bool isNewLineNeeded = true)
        {
            string result = string.Empty;
            if (isNewLineNeeded) result += "\n";
            for (int i = 0; i < tabCount; i++) result += "\t";
            return result;
        }

        int GetAutoGeneratedInsertionIndex(string fileContent, bool isForAPIController, out bool isEmptyInside)
        {
            if (isForAPIController)
            {
                index1 = fileContent.LastIndexOf(autoGenStartRegion);
                index2 = fileContent.LastIndexOf(autoGenEndRegion);
            }
            else if (fileContent.Contains("public class HttpService"))
            {
                index1 = fileContent.IndexOf(string.Concat(autoGenStartRegion, " ", methodType));
                index2 = fileContent.IndexOf(autoGenEndRegion, index1);
            }
            else
            {
                index1 = fileContent.IndexOf(autoGenStartRegion);
                index2 = fileContent.IndexOf(autoGenEndRegion, index1);
            }
            isEmptyInside = false;
            //isEmptyInside = string.IsNullOrEmpty(fileContent.Substring(index1 + 22, index2 - index1 - 22).Trim());
            return index2;
        }

        bool validated()
        {
            isVMused = !string.IsNullOrWhiteSpace(txtViewModel.Text);
            isMappedVM = !string.IsNullOrWhiteSpace(txtVMProperties.Text);
            if (string.IsNullOrEmpty(txtMethodName.Text.Trim()) || string.IsNullOrEmpty(txtReturnType.Text.Trim()) || string.IsNullOrEmpty(txtServiceName.Text.Trim())) return false;
            else if (!string.IsNullOrEmpty(txtInputParams.Text.Trim()))
            {
                inputParams = txtInputParams.Text.Trim();
                parameters = new Dictionary<string, string>();
                string[] temp;

                foreach (string property in inputParams.Split(',').Select(s => s.Trim()))
                {
                    temp = property.Split(' ');
                    if (temp.Length != 2) return false;
                    parameters.Add(temp[1], temp[0]);
                }
                paramCount = parameters.Count;
                if (paramCount > 0 && isVMused && isMappedVM)
                {
                    vmProperties = txtVMProperties.Text.Trim().Split(',').Select(s => s.Trim()).ToList();
                    if (vmProperties.Count != paramCount) return false;
                }
            }
            return true;
        }

        void InsertAutoGeneratedContentIntoFile(string subFilePath, bool isForAPIController = false)
        {
            bool isEmptyInside;
            filePath = location + subFilePath;
            fileContent = File.ReadAllText(filePath);
            int index = GetAutoGeneratedInsertionIndex(fileContent, isForAPIController, out isEmptyInside);
            if (isEmptyInside) tempContent = string.Concat(GetNewLineAndTab(1, false), tempContent);
            fileContent = fileContent.Insert(index, tempContent);

            File.WriteAllText(filePath, fileContent);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                txtOutput.Text = string.Empty;

                if (validated())
                {
                    isWriteToFile = chkWriteToFile.Checked;
                    methodName = txtMethodName.Text.Trim();
                    if (methodName.Contains("Async")) methodName = methodName.Substring(0, methodName.IndexOf("Async"));
                    returnType = txtReturnType.Text.Trim();
                    serviceName = txtServiceName.Text.Trim();
                    viewModelName = txtViewModel.Text.Trim();
                    methodType = cboMethodType.SelectedItem.ToString();

                    switch (methodType)
                    {
                        case "Basic":
                            accessLayerServiceName = "BasicDataRequestManager";
                            wcfServiceFileName = "TheSTS.WCFServices\\BasicDataService.cs";
                            clientServiceFileName = "TheSTS\\APIService\\BasicService.cs";
                            clientInterfaceFileName = "TheSTS\\ServiceRef\\IBasicDataService.cs";
                            break;
                        case "Transaction":
                            accessLayerServiceName = "TransactionRequestManager";
                            wcfServiceFileName = "TheSTS.WCFServices\\TransactionService.cs";
                            clientServiceFileName = "TheSTS\\APIService\\TransactionService.cs";
                            clientInterfaceFileName = "TheSTS\\ServiceRef\\ITransactionService.cs";
                            break;
                        case "Report":
                            accessLayerServiceName = "ReportRequestManager";
                            wcfServiceFileName = "TheSTS.WCFServices\\ReportDataService.cs";
                            clientServiceFileName = "TheSTS\\APIService\\ReportService.cs";
                            clientInterfaceFileName = "TheSTS\\ServiceRef\\IReportDataService.cs";
                            break;
                    }
                    accessLayerFileName = string.Format("API.RequestManagement\\{0}.cs", accessLayerServiceName);
                    apiControllerFileName = string.Format("TheSTS.WebAPI.Core\\Controllers\\{0}ServiceController.cs", methodType);
                    wcfServiceInterfaceFileName = wcfServiceFileName.Insert(19, "I");

                    httpActionType = cboHTTPActionType.SelectedItem.ToString();
                    accessLayerMethodName = string.Concat("Manage", methodName, "RequestAsync");
                    accessLayerMethodName = accessLayerMethodName.Replace("RequestRequest", "Request");
                    clientVMmapping = variables = vmVariables = extractedVariables = generatedContent = string.Empty;
                    viewModelVariableName = viewModelName + " requestVM";

                    index = 0;
                    foreach (KeyValuePair<string, string> parameter in parameters ?? new Dictionary<string, string>())
                    {
                        dataType = parameter.Value;
                        variable = parameter.Key;

                        variables += variable;

                        if (isVMused)
                        {
                            vmVariable = isMappedVM ? vmProperties[index] : string.Concat(variable.Substring(0,1).ToUpper(), variable.Substring(1));
                            vmVariables += string.Format("requestVM.{0}", vmVariable);
                            clientVMmapping += string.Format("{0} = {1}", vmVariable, variable);
                        }
                        if (index < paramCount - 1)
                        {
                            variables += ", ";
                            if (isVMused)
                            {
                                vmVariables += ", ";
                                clientVMmapping += string.Concat(",", GetNewLineAndTab(4));
                            }
                        }

                        extractedVariables += string.Concat(GetNewLineAndTab(4), "", dataType, " ", variable, " = request.ExtractDataOfProperty<", dataType, ">(", (paramCount > 0 ? string.Concat("\"", variable, "\"") : ""), ");");
                        index++;
                    }

                    #region Access Layer

                    tempContent = string.Empty;
                    tempContent += string.Concat("public static async Task<", returnType, "> ", accessLayerMethodName, "(", (isVMused ? viewModelVariableName : inputParams), ")", GetNewLineAndTab(2), "{");
                    tempContent += string.Concat(GetNewLineAndTab(3), "return await new ", serviceName, "().", methodName, "Async(", (isVMused ? vmVariables : variables), ");", GetNewLineAndTab(2), "}", GetNewLineAndTab(0), GetNewLineAndTab(2));

                    if (isWriteToFile) InsertAutoGeneratedContentIntoFile(accessLayerFileName);
                    generatedContent += string.Concat(tempContent, "\n");

                    #endregion

                    #region WCF & HTTP Service

                    tempContent = string.Empty;
                    tempContent += string.Concat("public Task<", returnType, "> ", methodName, "(", (isVMused ? viewModelVariableName : inputParams), ")", GetNewLineAndTab(2), "{");
                    tempContent += string.Concat(GetNewLineAndTab(3), "return CreateResponseAsync(async () =>", GetNewLineAndTab(3), "{");
                    tempContent += string.Concat(GetNewLineAndTab(4), "return await ", accessLayerServiceName, ".", accessLayerMethodName, "(", (isVMused ? "requestVM" : variables), ");", GetNewLineAndTab(3), "});", GetNewLineAndTab(2), "}", GetNewLineAndTab(0), GetNewLineAndTab(2));

                    if (isWriteToFile)
                    {
                        InsertAutoGeneratedContentIntoFile(wcfServiceFileName);
                        InsertAutoGeneratedContentIntoFile(httpServiceFileName);
                    }
                    generatedContent += string.Concat(tempContent, "\n");

                    #endregion

                    #region WCF Service Interface

                    tempContent = string.Empty;
                    tempContent += string.Concat("[OperationContract]", GetNewLineAndTab(2), "Task<", returnType, "> ", methodName, "(", (isVMused ? viewModelVariableName : inputParams), ");", GetNewLineAndTab(0), GetNewLineAndTab(2));

                    if (isWriteToFile) InsertAutoGeneratedContentIntoFile(wcfServiceInterfaceFileName);
                    generatedContent += string.Concat(tempContent, "\n");

                    #endregion

                    #region Route

                    tempContent = string.Empty;
                    tempContent = string.Concat(GetNewLineAndTab(2), "public const string ", "Route_", methodType, "_", methodName, " = \"", methodName.Substring(0, 1).ToLower(), methodName.Substring(1), "\";");
                    generatedContent += string.Concat(tempContent, "\n");

                    if (isWriteToFile)
                    {
                        filePath = location + routeFileName;
                        fileContent = File.ReadAllText(filePath);

                        index1 = fileContent.IndexOf(string.Format("#region {0}", methodType));
                        index2 = fileContent.IndexOf("#region Child", index1);
                        index3 = fileContent.IndexOf("#endregion", index2);
                        index4 = fileContent.LastIndexOf(";", index3 - 6);
                        fileContent = fileContent.Insert(index4 + 1, tempContent);

                        File.WriteAllText(filePath, fileContent);
                    }

                    #endregion

                    #region Client Interface

                    tempContent = string.Empty;
                    tempContent += string.Concat("void ", methodName, "(", (isVMused ? viewModelVariableName : inputParams), (string.IsNullOrEmpty(inputParams) ? "" : ", "), "OnCompletion<", returnType, "> result);", GetNewLineAndTab(0), GetNewLineAndTab(2));

                    if (isWriteToFile) InsertAutoGeneratedContentIntoFile(clientInterfaceFileName);
                    generatedContent += string.Concat(tempContent, "\n");

                    #endregion

                    #region Client Service

                    tempContent = string.Empty;
                    tempContent += string.Concat("public static async Task ", methodName, "Async (", inputParams, (string.IsNullOrEmpty(inputParams) ? "" : ", "), "OnCompletion<", returnType, "> callback)", GetNewLineAndTab(2), "{");
                    if (isVMused)
                    {
                        tempContent += string.Concat(GetNewLineAndTab(3), "", viewModelName, " vm = new ", viewModelName, GetNewLineAndTab(3), "{");
                        tempContent += string.Concat(GetNewLineAndTab(4), "", clientVMmapping, GetNewLineAndTab(3), "};");
                    }
                    tempContent += string.Concat(GetNewLineAndTab(3), "if (Config.ApiEndPointType == EnumAPIEndpointType.WCF)", GetNewLineAndTab(3), "{");
                    tempContent += string.Concat(GetNewLineAndTab(4), "ServiceProxy.", methodName, "(", (isVMused ? "vm" : variables), (string.IsNullOrEmpty(inputParams) ? "" : ", "), "(s, e) => CompleteWCFRequest(ServiceProxy, s, e, callback));", GetNewLineAndTab(3), "}");
                    tempContent += string.Concat(GetNewLineAndTab(3), "else", GetNewLineAndTab(3), "{");
                    tempContent += string.Concat(GetNewLineAndTab(4), "await ProcessRequest(apiPrefix, EnumHTTPVerb.", httpActionType, ", RouteBucket.Route_", methodType, "_", methodName, ", ", (isVMused ? "vm" : string.Concat("new { ", variables, " }")), " , callback);", GetNewLineAndTab(3), "}", GetNewLineAndTab(2), "}", GetNewLineAndTab(0), GetNewLineAndTab(2));

                    if (isWriteToFile) InsertAutoGeneratedContentIntoFile(clientServiceFileName);
                    generatedContent += string.Concat(tempContent, "\n");

                    #endregion

                    #region API Controller

                    tempContent = string.Empty;
                    tempContent += string.Concat("[Route(RouteBucket.Route_", methodType, "_", methodName, ")]");
                    tempContent += string.Concat(GetNewLineAndTab(2), "[Http", httpActionType.Substring(0, 1), httpActionType.Substring(1).ToLower(), "]");
                    tempContent += string.Concat(GetNewLineAndTab(2), "public async Task<HttpResponseMessage> ", methodName, "Async(" , (httpActionType != "GET" ? "[FromBody]JObject request" : ""), ")", GetNewLineAndTab(2), "{");
                    tempContent += string.Concat(GetNewLineAndTab(3), "return await Request.CreateHttpResponseAsync(async () =>", GetNewLineAndTab(3), "{");

                    if (!string.IsNullOrEmpty(variables))
                    {
                        tempContent += string.Concat(GetNewLineAndTab(4), "#region Variable Extraction");

                        if (isVMused) tempContent += string.Concat(GetNewLineAndTab(4), "", viewModelName, " requestVM = request.ExtractDataOfProperty<", viewModelName, ">();");
                        else tempContent += extractedVariables;

                        tempContent += string.Concat(GetNewLineAndTab(4), "#endregion");
                    }

                    tempContent += string.Concat(GetNewLineAndTab(4), "var value = await ", accessLayerServiceName, ".", accessLayerMethodName, "(", (isVMused ? "requestVM" : variables), ");");
                    tempContent += string.Concat(GetNewLineAndTab(4), "return Request.CreateResponse(HttpStatusCode.OK, value);", GetNewLineAndTab(3), "});", GetNewLineAndTab(2), "}", GetNewLineAndTab(0), GetNewLineAndTab(2));

                    if (isWriteToFile) InsertAutoGeneratedContentIntoFile(apiControllerFileName, true);
                    generatedContent += string.Concat(tempContent, "\n");

                    #endregion

                    txtOutput.Text = generatedContent;
                }
                else MessageBox.Show("Please fill-up required fields");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}